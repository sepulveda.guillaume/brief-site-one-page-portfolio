// Script pour cacher et faire appararaître le menu de navigation

let buttonNav = document.querySelector(".mobile-nav-toggle");
let openNav = document.querySelector(".header-mobile");

buttonNav.addEventListener("click", openNavbar);

function openNavbar(e) {
  let testClass = openNav.classList;
  if (testClass == "header-mobile") {
    openNav.classList.replace("header-mobile", "header-mobile-active");
    e.target.classList.value = "bi bi-x mobile-nav-toggle";
  }
  else {
    openNav.classList.replace("header-mobile-active", "header-mobile");
    e.target.classList.value = "bi bi-list mobile-nav-toggle";
  }
}


// Script pour mettre en case active l'onglet de navigation

let navButtons = document.querySelectorAll(".nav-link");

for (let i = 0; i < navButtons.length; i++) {
  navButtons[i].addEventListener("focus", function() {
    let current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}

//Script pour effet titre hover

  var textWrapper = document.querySelector('.ml11 .letters');
  textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

  anime.timeline({loop: true})
    .add({
      targets: '.ml11 .line',
      scaleY: [0,1],
      opacity: [0.5,1],
      easing: "easeOutExpo",
      duration: 700
    })
    .add({
      targets: '.ml11 .line',
      translateX: [0, document.querySelector('.ml11 .letters').getBoundingClientRect().width + 10],
      easing: "easeOutExpo",
      duration: 700,
      delay: 100
    }).add({
      targets: '.ml11 .letter',
      opacity: [0,1],
      easing: "easeOutExpo",
      duration: 600,
      offset: '-=775',
      delay: (el, i) => 34 * (i+1)
    }).add({
      targets: '.ml11',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
    });


// Script pour effet texte taped

//Code général

class TextScramble {
  constructor(el) {
    this.el = el
    this.chars = '!<>-_\\/[]{}—=+*^?#________'
    this.update = this.update.bind(this)
  }
  setText(newText) {
    const oldText = this.el.innerText
    const length = Math.max(oldText.length, newText.length)
    const promise = new Promise((resolve) => this.resolve = resolve)
    this.queue = []
    for (let i = 0; i < length; i++) {
      const from = oldText[i] || ''
      const to = newText[i] || ''
      const start = Math.floor(Math.random() * 40)
      const end = start + Math.floor(Math.random() * 40)
      this.queue.push({ from, to, start, end })
    }
    cancelAnimationFrame(this.frameRequest)
    this.frame = 0
    this.update()
    return promise
  }
  update() {
    let output = ''
    let complete = 0
    for (let i = 0, n = this.queue.length; i < n; i++) {
      let { from, to, start, end, char } = this.queue[i]
      if (this.frame >= end) {
        complete++
        output += to
      } else if (this.frame >= start) {
        if (!char || Math.random() < 0.28) {
          char = this.randomChar()
          this.queue[i].char = char
        }
        output += `<span class="dud">${char}</span>`
      } else {
        output += from
      }
    }
    this.el.innerHTML = output
    if (complete === this.queue.length) {
      this.resolve()
    } else {
      this.frameRequest = requestAnimationFrame(this.update)
      this.frame++
    }
  }
  randomChar() {
    return this.chars[Math.floor(Math.random() * this.chars.length)]
  }
}

//Exemple 

const phrases = [
  'Je suis',
  'développeur web et web mobile',
]

const el = document.querySelector('.text_taped')
const fx = new TextScramble(el)

let counter = 0
const next = () => {
  fx.setText(phrases[counter]).then(() => {
    setTimeout(next, 800)
  })
  counter = (counter + 1) % phrases.length
}

next()


// Script filtre projects

let buttonFilter = document.querySelectorAll('#filtres li');

buttonFilter.forEach(element => {
  element.addEventListener('click', function() {
      let cleanFilter = document.querySelectorAll('#filtres li')
      cleanFilter.forEach(removeElement => {
        removeElement.classList.remove("active");
      })
      filterItems(element);
      })
  });

function filterItems(varElement) {
  let projectsContent = document.querySelectorAll('.filter-item');
  let filterActive = varElement.classList;

  filterActive.add("active");

  let stringToGoIntoTheRegex = varElement.dataset.filter;
  let regex = new RegExp(stringToGoIntoTheRegex, "g");

  projectsContent.forEach(element => {
    let elementContent = element.className;
    let found = elementContent.match(regex);
  
    if (found !== null) {
      element.style.display = "block";
    }
    else {
      element.style.display = "none"  
    }
})
}

